class work {

    constructor() {
        this.paybalance = 0;
    }

    doTheWork(){
        this.paybalance += 100;
    }

    payTheMan(bank){
        if (bank.activeLoan){
            bank.customerBalance += (this.paybalance * 0.90);
            bank.payBackLoan(this.paybalance * 0.10);
            this.paybalance = 0;
        }
        else {
            bank.customerBalance += this.paybalance;
            this.paybalance = 0;
        }
    }

}

export default work;