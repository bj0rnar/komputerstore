class bank {

    activeLoan = false;
    aLoanListener = function () {}

    constructor(customerName) {
        this.customerName = customerName
        this.customerBalance = 0;
        this.currentLoan = 0;
    }

    get balance(){
        return this.customerBalance;
    }

    get loan(){
        return this.currentLoan;
    }

    setActiveLoan(val){
        this.activeLoan = val;
        this.aLoanListener();
    }

    setListener(listener){
        this.aLoanListener = listener;
    }


    get activeLoan(){
        return this.activeLoan;
    }

    payBackLoan(paidAmount){
        if (paidAmount > 0 && this.currentLoan !== 0){
            if (paidAmount >= this.currentLoan){
                const remainder = paidAmount - this.currentLoan;
                this.customerBalance += remainder;
                this.currentLoan = 0;
                this.setActiveLoan(false);
            }
            else {
                this.currentLoan -= paidAmount;
            }
        }
        else {
            console.log("payback pay: " + paidAmount);
            console.log("payback loan: " + this.currentLoan);
            alert('illegal amount')
        }
    }

    applyForLoan(desiredAmount){

        if (this.activeLoan === true){
            alert('currently active loan')
            return false;
        }

        if (desiredAmount > (this.customerBalance*2)){
            alert("you cannot afford this loan")
            return false;
        }

        this.customerBalance += desiredAmount;
        this.currentLoan += desiredAmount;
        this.setActiveLoan(true);
        return true;
    }
}
export default bank;
