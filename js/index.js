import computers from './laptops.js'
import bank from "./bank.js";
import work from "./work.js"

//bank stuff
const bankerName = document.getElementById("customer-name-element");
const bankBalanceElement = document.getElementById('balance-element');
const bankLoanElement = document.getElementById('customer-loan-element')
const bankLoanButton = document.getElementById('customer-loan-button');

//work and cashout buttons
const workButton = document.getElementById('work-button');
const workBalanceElement = document.getElementById('work-balance');
const cashoutButton = document.getElementById('cashout-button');
const payBankButton = document.getElementById('work-pay-loan-button');

//Features and select list
const selectComputers = document.getElementById('select-computers');
const selectComputersFeatures = document.getElementById('select-computers-features');

//Computer display stuff
const computerInfoHeader = document.getElementById('computer-name');
const computerInfoPrice = document.getElementById('computer-price');
const computerInfoDescription = document.getElementById('computer-description');
const computerInfoStock = document.getElementById('computer-stock');
const computerBuyButton = document.getElementById('computer-buy-button');
const computerImage = document.getElementById('image-element');


//Initialize stuff and hide pay button.
const workObject = new work();
const bankObject = new bank('Scrooge McDuck');
bankerName.innerText = bankObject.customerName;
payBankButton.style.display = 'none';

//helper method to populate the initial list
(function () {
    selectComputers.value = 0;
    const computer = computers[0];
    populateList(computer);
})();



//set observer to watch the "activeLoan" boolean
bankObject.setListener(function () {
    if (payBankButton.style.display === "none") {
        payBankButton.style.display = "block";
    }
    else {
        payBankButton.style.display = "none";
    }
})


//populate select array
computers.forEach(function (element, index) {
    let option = document.createElement('option');

    console.log(index);
    option.value = index;
    option.text = element.name;

    selectComputers.appendChild(option);
})



//changelistener for dropdown menu
selectComputers.addEventListener('change', function (event) {
    selectComputersFeatures.innerHTML = "";

    //update stuff
    const el = computers[event.target.value];
    populateList(el);
})

//helper method to populate the info
function populateList(pc) {
    computerInfoHeader.innerText = pc.name;
    computerInfoPrice.innerText = pc.price;
    computerInfoDescription.innerText = pc.description;
    computerInfoStock.innerText = pc.stock;
    computerImage.src = pc.imageUrl;
    console.log(pc.imageUrl);

    //Add features to list
    for (let i = 0; i < pc.features.length; i++) {
        const li = document.createElement('li');
        const txt = document.createTextNode(pc.features[i]);
        li.appendChild(txt);

        selectComputersFeatures.appendChild(li);
    }
}



/**
 * DOWN HERE BE BUTTONS
 */
workButton.addEventListener('click', function () {
    workObject.doTheWork();
    workBalanceElement.innerText = workObject.paybalance.toString() + " kr";
})

cashoutButton.addEventListener('click', function () {
    if (workObject.paybalance > 0) {
        workObject.payTheMan(bankObject);

        workBalanceElement.innerText = workObject.paybalance.toString() + " kr";
        bankBalanceElement.innerText = "Balance: " + bankObject.customerBalance.toString() + " kr";
        bankLoanElement.innerText = "Loan: " + bankObject.currentLoan.toString() + " kr";
    }
})

bankLoanButton.addEventListener('click', function () {
    let loan = prompt("Apply for loan:", "yay")
    if (loan != null) {
        if (!isNaN(loan)) {
            const sum = parseInt(loan);
            console.log("sum" + sum)
            if (bankObject.applyForLoan(sum)) {
                bankLoanElement.innerText = "Loan: " + bankObject.currentLoan.toString();
                bankBalanceElement.innerText = "Balance: " + bankObject.customerBalance.toString() + " kr";
            }
        } else {
            alert("invalid input")
        }
    }
})

payBankButton.addEventListener('click', function () {
    if (workObject.paybalance > 0){
        bankObject.payBackLoan(workObject.paybalance);
        workObject.paybalance = 0;

        workBalanceElement.innerText = workObject.paybalance.toString() + " kr";
        bankBalanceElement.innerText = "Balance: " + bankObject.customerBalance.toString() + " kr";
        bankLoanElement.innerText = "Loan: " + bankObject.currentLoan.toString() + " kr";
    }
})

computerBuyButton.addEventListener('click', function () {
    const currentOption = selectComputers.options[selectComputers.selectedIndex].text
    const computer = computers.find(x => x.name === currentOption);
    if ((bankObject.customerBalance >= computer.price) && computer.stock !== 0){
        bankObject.customerBalance -= computer.price;
        computerInfoStock.innerText = (computers.find(x => x.name === currentOption).stock -= 1).toString();
        bankBalanceElement.innerText = "Balance: " + bankObject.customerBalance.toString() + " kr";
        alert('Congratulations of your purchase of ' + computer.name);
    }
    else {
        alert("You can't afford this laptop")
    }
})


