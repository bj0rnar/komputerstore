const computers = [
    {
        "name": "AlienWare Supercomputer",
        "description": "This is the realest stuff you'll ever see",
        features: ["Reasonly priced", "Can run doom", "Looks cool"],
        "imageUrl": "https://i.dell.com/is/image/DellContent//content/dam/global-asset-library/Products/Notebooks/Alienware/m17_r3_non-touch_non-tobii/awm17_r3_cnb_00055lf110_gy.psd?fmt=pjpg&pscan=auto&scl=1&hei=402&wid=575&qlt=95,0&resMode=sharp2&op_usm=1.75,0.3,2,0&size=575,402",
        "price": 25000,
        "stock": 12
    },
    {
        "name": "Macintosh X10",
        "description": "Just an average MAC with average components",
        features: ["Priced after market value", "Gain hipster status", "Same components as a regular computer at double the price!"],
        "imageUrl": "https://img.prisguiden.no/822/822716/Apple-MacBook-Pro-13.3-inch-2.4-GHz1.1024x768m.jpg",
        "price": 60000,
        "stock": 7
    },
    {
        "name": "The Experis Work Laptop",
        "description": "This baby will run smoothly for about 2 weeks!",
        features: ["Given for free on getting a job!", "Comes with a bag that's too big", "You'll at least get to hang out with Nick and Dewald"],
        "imageUrl": "https://i.redd.it/bnqxavsvnit11.jpg",
        "price": 400,
        "stock": 16
    },
    {
        "name": "El Schnell Prototype",
        "description": "ALIENWARE IS FAST, MACINTOSH IS LOUD, EXPERIS IS SMOOTH, BUT ONLY EL SCHNELL IS GOD",
        features: ["Gives divine powers upon purchase", "An unstoppable force", "An immovable object"],
        "imageUrl": "https://i.pinimg.com/originals/7f/45/70/7f457078fdbfe0d7994dd5f67fd150d8.jpg",
        "price": 99999,
        "stock": 1
    }
];

export default computers;